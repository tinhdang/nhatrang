<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/header.php';?>
  <!--  -->
  <div class="content_main">
    <img src="image/background.png" class="img-rensponsive img_background">
    <div class="container content_center ">
      <div class="order_room ">
        <p class="title_order"> Đặt phòng </p>
        <form class="form_order" id="booking" method="post" action="booking.php" name="room_order" onsubmit="return ValidateRoom()">
          <div class="order1 col-sm-6 ">
            <input type="text" class="datepicker_arr" name="date_arr" id="ip_datearr" readonly="true"  placeholder="Ngày đến" value="">
          </div>
          <div class="order2 col-sm-6 ">
            <input type="text" class="datepicker_dep" id="ip_datedep" name="date_dep" readonly="true" placeholder="Ngày đi" value="">
          </div>
          <div style="clear: both;"></div>
          <div class="order3 col-sm-6 ">
          <?php $tuoi=range(1,10);?>
            <select name="name_children" id="ip_children">
              <option value="">Người lớn</option>
                <?php 
                  $i=1;
                  foreach ($tuoi as $value) {
                    $t=$tuoi[$i-1];
                    $i++;
                ?>
              <option value="<?php echo $t?>"><?php echo $t;?></option>
                <?php  } ?>
            </select>
          </div>
          <div class="order4 col-sm-6 ">
            <select name="name_adult" id="ip_adult">
              <option value="">Trẻ em</option>
              <?php 
                  $i=1;
                  foreach ($tuoi as $value) {
                    $t=$tuoi[$i-1];
                    $i++;
                ?>
              <option value="<?php echo $t?>"><?php echo $t;?></option>
                <?php  } ?>
            </select>        
          </div>
          <div class="order5">
            <input type="submit" name="sb_booking" value="ĐẶT PHÒNG">
          </div>
        </form>
        <!-- Lấy DL -->
        <?php 
        if(isset($_POST['sb_booking']))
          { 
              $date_arr=$_POST['date_arr'];
              $date_dep=$_POST['date_dep'];
              $name_children=$_POST['name_children'];
              $name_adult=$_POST['name_adult']; 
          }
        ?>
        <span class="caret-booking"></span>
      </div>
      <div class="pic_under">
        <div class="row">
          <div class="w3-display-container w3-text-white div_img col-xs-3">
            <img src="image/pic1.png" class=" img_under ">
            <div class="w3-display-bottommiddle w3-container"><p>NHÀ HÀNG ROYAL RESTAURANT</p></div>
          </div>
          <div class="w3-display-container w3-text-white div_img col-xs-3">
            <img src="image/pic2.png" class=" img_under">
            <div class="w3-display-bottommiddle w3-container"><p>NHÀ HÀNG ROYAL PALACE</p></div>
          </div>
          <div class="w3-display-container w3-text-white div_img col-xs-3">
            <img src="image/pic3.png" class=" img_under ">
            <div class="w3-display-bottommiddle w3-container"><p>THE MOON BAR</p></div>
          </div>
          <div class="w3-display-container w3-text-white div_img col-xs-3">
            <img src="image/pic4.png" class=" img_under ">
            <div class="w3-display-bottommiddle w3-container"><p class="">BEACH BAR FANTASY</p></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="link_mxh container">
    <div class="row">
      <p class="col-sm-1"></p>
      <p class="img_link col-sm-2">
        <img src="image/facebook.png" class="img-rensponsive">
      </p>
      <p class="img_link col-sm-2">
        <img src="image/google.png" class="img-rensponsive">
      </p>
      <p class="img_link col-sm-2">
        <img src="image/youtube.png" class="img-rensponsive">
      </p>
      <p class="img_link col-sm-2">
        <img src="image/tripadvisor.png" class="img-rensponsive">
      </p>
      <p class="img_link col-sm-2">
        <img src="image/virtual.png" class="img-rensponsive">
      </p>
    </div>
  </div>
  <?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/footer.php';?>
