<?php ob_start();?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/functions/dbconnect.php';?>
<div class="content_main checkout_main">
  <div class="checkout container">
    <h3 >CHECKOUT</h3>
    <h4>Tìm thông tin người đặt</h4>
    <form method="post" action="" id="form_search">
      <div class="row form-group">
        <label class="control-label col-sm-3 col-sm-offset-1">Họ và tên <span style="color: red">(*)</span></label>
        <input class="col-sm-4" id="" type="text" name="username" value="">
      </div>
      <div class="row form-group">
        <label class="control-label col-sm-3 col-sm-offset-1">Số CMND <span style="color: red">(*)</span></label>
        <input class="col-sm-4" id="" type="number" name="idcard" value="">
      </div>
      <div class="row form-group send">
        <input type="submit" class="send_sm" name="search" value="Search" onclick="myFunction()" >
      </div>
    </form>
    <?php     
        if(isset($_POST['search'])){

        $username=$connect->real_escape_string($_POST['username']);
        $id_card=$connect->real_escape_string($_POST['idcard']);
        $sql="SELECT * FROM booking WHERE name='$username' AND id_card='$id_card' ORDER BY id_bk DESC";
        $result=$connect->query($sql);
        $num = mysqli_num_rows($result);
        if($num!=0){
          header("LOCATION:check.php?id={$id_card}&&username={$username}");
        exit();
        }else {
          echo 'Không tìm thấy dữ liệu được tìm kiếm';
        }
       ?>
       <?php
        }
    ?>
  </div>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/footer.php';?>

