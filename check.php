<?php ob_start(); ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/functions/dbconnect.php';?>
<div class="content_main checkout_main">
  <div class="checkout container">
    <h3>KẾT QUẢ TÌM KIẾM</h3>
    <?php
    if(isset($_GET['id'])&&isset($_GET['username'])){
      $username=$connect->real_escape_string($_GET['username']);
      $id_card=$_GET['id'];
      $sql="SELECT * FROM booking WHERE name='$username' AND id_card='$id_card' ORDER BY id_bk DESC";
      $result=$connect->query($sql);
      $num = mysqli_num_rows($result);
       if($num==0){
        header("LOCATION:checkout.php");
          exit;
      }
      while ($arr_search=mysqli_fetch_assoc($result)){
        if($num==0){
          echo 'Không tìm thấy ';
          }else {
          ?>
        <div class="form_checkout">
          <h4>Thông tin đặt phòng  </h4>
          <?php
            $id_bk=$arr_search['id_bk'];
            $id_room=$arr_search['id_room'];
            $rooms=$arr_search['rooms'];
            $arr_date=$arr_search['arr_date'];
            $dep_date=$arr_search['dep_date'];
            $num_children=$arr_search['num_children'];
            $num_adult=$arr_search['num_adult'];
            $name=$arr_search['name'];
            $gender=$arr_search['gender'];
            $id_card=$arr_search['id_card'];
            $address=$arr_search['address'];
            $email=$arr_search['email'];
            $phone=$arr_search['phone'];
            $note=$arr_search['note'];
            $status=$arr_search['status'];
           //Tách ngày tháng năm     
            $arr_new=explode ('-', $arr_date);
            $dep_new=explode ('-', $dep_date);
            /*Đưa về định dạng ngày tháng năm*/
            $nam_arr=$arr_new[0];
            $thang_arr=$arr_new[1];
            $ngay_arr=$arr_new[2];
            $nam_dep=$dep_new[0];
            $thang_dep=$dep_new[1];
            $ngay_dep=$dep_new[2];
            $arr_arr=array(
              '0' =>$ngay_arr ,  
              '1' =>$thang_arr , 
              '2' =>$nam_arr , 
              );
            $arr_dep=array(
              '0' =>$ngay_dep ,  
              '1' =>$thang_dep, 
              '2' =>$nam_dep, 
              );
            $checkin=implode('-',$arr_arr);
            $checkout=implode('-',$arr_dep);
            if($status==0){
              $sta='Chưa trả';
            }else{
              $sta='Đã trả';
            }
          ?>
          <div class="row form-group">
            <div class="table-responsive">
              <table class="w3-table-all w3-hoverable">
                <thead>
                  <tr class="w3-light-grey" style="font-weight: bold;">
                    <th>Id_bk</th>
                    <th>Tên KH</th>
                    <th>Địa chỉ</th>
                    <th>Điện thoại</th>
                    <th>Số phòng </th>
                    <th>Loại phòng </th>
                    <th>Thời gian </th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $id_bk;?></td>
                    <td><?php echo $name;?></td>
                    <td><?php echo $address;?></td>
                    <td><?php echo $phone;?></td>
                    <td><?php echo $rooms;?></td>
                      <?php 
                        $sqli="SELECT * FROM cate_room WHERE id_cate='$id_room'";
                        $res=$connect->query($sqli);
                        $row=mysqli_fetch_assoc($res);
                        $type_room=$row['type_room'];
                        $id_cate=$row['id_cate'];
                        $room_bl=$row['room_bl'];
                        $room_blank=$rooms+$room_bl;
                      ?>
                    <td><?php echo $type_room;?></td>
                    <td><?php echo 'Từ ngày '.$checkin.' đến ngày '.$checkout;?></td>
                    <td>
                      <?php
                      if($status==1){
                        echo $sta;}else{?>
                      <form method="post" action="">
                        <input type="text" readonly="" name="nameco" value="<?php echo $id_bk;?>" style="display: none;">
                        <input type="submit" class="" name="checkout" value="Checkout">
                      </form>
                      <?php 
                      /*Xử lý trả phòng*/
                      if(isset($_REQUEST['checkout'])){
                        $con=mysqli_connect("localhost","root","","nhatrang");
                        mysqli_set_charset($con,"utf8");
                        $id_co=$_POST['nameco'];
                        $update="UPDATE booking SET status='1' WHERE id_bk='$id_co'";
                        $reup = mysqli_query($con,$update);
                        if($reup){
                            /**/
                             //Update lại số phòng trống sau khi trả
                              $up="UPDATE cate_room SET room_bl='$room_blank' WHERE id_cate='$id_cate'";
                              $re_up = mysqli_query($con,$up);
                               header("LOCATION:check.php?id={$id_card}&&username={$username}&&msg=Checkout thành công");
                               exit();
                              }else{
                              echo '<script type="text/javascript">alert("Trả phòng không thành công");</script>';
                            }
                        }

                      ?>
                        <?php }?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php      
          }
        }    
      }
      ?>
      <?php 
    if(isset($_GET['msg'])){
          echo '<b><span style="color:red">'.$_GET['msg'].'</span></b>';
        }
    ?>
  </div>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/footer.php';?>
