-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2016 at 04:40 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nhatrang`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id_bk` int(255) NOT NULL,
  `id_room` int(255) NOT NULL,
  `rooms` int(255) NOT NULL,
  `arr_date` date NOT NULL,
  `dep_date` date NOT NULL,
  `num_children` int(255) NOT NULL,
  `num_adult` int(255) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_card` int(10) NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(255) NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id_bk`, `id_room`, `rooms`, `arr_date`, `dep_date`, `num_children`, `num_adult`, `name`, `gender`, `id_card`, `address`, `email`, `phone`, `note`, `status`) VALUES
(1, 2, 3, '2016-10-22', '2016-10-28', 2, 0, 'Nguyen Van A', 'Nam', 123456789, 'Da Nang', 'nva@gmail.com', 905921933, 'No cmt', 0),
(2, 1, 2, '2016-10-20', '2016-10-22', 1, 0, 'Nguyen Van C', 'Nữ', 222333444, 'Quang Nam', 'nvb@gmail.com', 935334842, 'No cmt', 0),
(3, 4, 5, '2016-10-28', '2016-10-29', 2, 9, 'Test01', 'Nữ', 987654321, 'Đà Nẵng', 'Test01@gmail.com', 905921933, 'Đặt phòng ', 1),
(4, 3, 5, '2016-10-29', '2016-10-31', 5, 5, 'Test02', 'Nam', 111222333, 'Đà Nẵng', 'Test02gmail.com', 905921933, 'Đặt phòng ', 1),
(6, 1, 1, '2016-10-29', '2016-10-29', 1, 1, 'Test03', 'Nữ', 11112222, 'Đà Nẵng', 'Test03@gmail.com', 905921933, 'Đặt phòng ', 0),
(7, 2, 4, '2016-10-29', '2016-10-31', 1, 5, 'Test04', 'Nam', 333444555, 'Đà Nẵng', 'Test04@gmail.com', 905921933, 'Đặt phòng ', 0),
(8, 1, 2, '2016-11-11', '2016-11-18', 3, 6, 'Test05', 'Nam', 123456, 'Đà Nẵng', 'dangthitinh@gmail.com', 909090909, 'Đặt phòng ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cate_room`
--

CREATE TABLE `cate_room` (
  `id_cate` int(255) NOT NULL,
  `type_room` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `total_rooms` int(255) NOT NULL,
  `room_bl` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cate_room`
--

INSERT INTO `cate_room` (`id_cate`, `type_room`, `total_rooms`, `room_bl`) VALUES
(1, 'Deluxe Room', 10, 5),
(2, 'Palace Club Suite', 5, 7),
(3, 'Palace Suite', 10, 5),
(4, 'Superior Seaview Room', 10, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id_bk`),
  ADD KEY `fk_room` (`id_room`);

--
-- Indexes for table `cate_room`
--
ALTER TABLE `cate_room`
  ADD PRIMARY KEY (`id_cate`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id_bk` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cate_room`
--
ALTER TABLE `cate_room`
  MODIFY `id_cate` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `fk_room` FOREIGN KEY (`id_room`) REFERENCES `cate_room` (`id_cate`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
