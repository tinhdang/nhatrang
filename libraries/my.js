/* Hide/Show box*/
$(document).click(function (e){
  var container = $(".login_form");
  if (!container.is(e.target) && container.has(e.target).length === 0){
      container.hide();
      }
     });
$("document").ready(function(){
  $(".login_user").click(function(){
    $(".login_form").toggle('fast');
      return false;
    });  
});
$("document").ready(function(){
  $(".form_min").click(function(){
    $(".order_room").slideToggle();
      return false;
    });  
  });
$("document").ready(function(){
  $(".title_1").click(function(){
    $(".ul_li_1").slideToggle();
      return false;
      });
    });
$("document").ready(function(){
  $(".title_2").click(function(){
    $(".ul_li_2").slideToggle();
    return false;
      });
  });
$("document").ready(function(){
  $(".title_3").click(function(){
    $(".ul_li_3").slideToggle();
      return false;
    });
  });
$("document").ready(function(){
  $(".title_4").click(function(){
    $(".ul_li_4").slideToggle();
    return false;
    });
  });
$("document").ready(function(){
  $(".title_5").click(function(){
    $(".ul_li_5").slideToggle();
      return false;
    });
  });
 
 $("document").ready(function(){
  $(".click_mess").click(function(){
    $(".form_message").slideToggle();
      return false;
    });  
  });
/*Validate Message*/
function ValidateMess(){
var text=document.forms["form_mess"]["user_mess"].value;
var content=document.forms["form_mess"]["content_mess"].value;
  if(text==null||text==""){
    alert("Vui lòng nhâp họ tên");
    return false;
}
if(content==null||content==""){
    alert("Vui lòng nhâp nội dung");
    return false;
}
}
/*Disable past day */
    // DatePicker
    // $("#arrival").datepicker({minDate: new Date(), dateFormat: 'dd/mm/yy'});

$( function() {
  $( ".datepicker" ).datepicker({
    minDate: '0',
    dateFormat: 'dd/mm/yy' 
  });
});
/*Validate form booking*/
$('document').ready(function(){
  $('#form_booking').submit(function(){
    var username=$.trim($('#ip_name').val());
    var address=$.trim($('#ip_address').val());
    var gender=$.trim($('#ip_gender').val());
    var type=$.trim($('#ip_type').val());
    var phone=$.trim($('#ip_phone').val());
    var email=$.trim($('#ip_email').val());
    var children=$.trim($('#ip_children').val());
    var adult=$.trim($('#ip_adult').val());
    var datearr=$.trim($('#ip_datearr').val());
    var datedep=$.trim($('#ip_datedep').val());
    var note=$.trim($('#ip_note').val());
    var rooms=$.trim($('#ip_rooms').val());
    var card=$.trim($('#ip_card').val());
    var flag=true;
    // 
    if(username==''||username.length<6){
      $('#name_error').html('<span style="color:red;font-size:12px;">&nbsp Vui lòng nhập họ tên, phải lớn hơn 6 ký tự<span');
    }else {
      $('#name_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }
    // 
    if(address==''){
      $('#address_error').html('<span style="color:red;font-size:12px;">&nbsp Vui lòng nhập địa chỉ </span>');
      flag=false;
    }else{
      $('#address_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }
    // 
   /* if(gender==''){
      $('#gender_error').html('<span style="color:red;font-size:13px">&nbspVui lòng chọn giới tính</span> ');
      flag=false;
    }else{
      $('#gender_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }*/
    if(type==''){
      $('#type_error').html('<span style="color:red;font-size:12px;">&nbspVui lòng chọn loại phòng</span> ');
      flag=false;
    }else{
      $('#type_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }

    if(isNaN(phone)||phone.length<10){
      $('#phone_error').html('<span style="color:red;font-size:12px;">&nbspSĐT sai định dạng</span> ');
      flag=false;
    }else{
      $('#phone_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }
    
    if(!isEmail(email)){
      $('#email_error').html('<span style="color:red;font-size:12px;">&nbspEmail sai định dạng</span>');
      flag=false;
    }else{
      $('#email_error').html('<span style="color:green;font-size:12px;">&nbsp&nbspOk</span>');
    }
    // 
    if(isNaN(card)||card==''){
      $('#card_error').html('<span style="color:red;font-size:12px;">&nbspVui lòng nhập CMND</span>');
      flag=false;
    }else{
      $('#card_error').html('<span style="color:green">&nbspOk</span>');
    }
    if(isNaN(adult)||adult==''){
      $('#adult_error').html('<span style="color:red;font-size:12px;">&nbspNhập số người lớn</span>');
      flag=false;
    }else{
      $('#adult_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }
  /*  if(rooms==0){
      $('#room_error').html('<span style="color:red;font-size:12px;">&nbspChọn số phòng</span>');
      flag=false;
    }else{
      $('#room_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }*/
    // 
    if(datearr==''){
      $('#datearr_error').html('<span style="color:red;font-size:12px;">&nbspVui lòng chọn ngày đến </span>');
      flag=false;
    }else{
      $('#datearr_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }
    if(datedep==''){
      $('#datedep_error').html('<span style="color:red;font-size:12px;">&nbspVui lòng chọn ngày đi</span> ');
      flag=false;
    }else{
      $('#datedep_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }
    //
    if(note==''){
      $('#note_error').html('<span style="color:red;font-size:12px;">&nbspVui lòng nhập ghi chú </span>');
      flag=false;
    }else{
      $('#note_error').html('<span style="color:green;font-size:12px;">&nbspOk</span>');
    }
    //
    return flag;
  });
});
/* Function kiểm tra email*/
function isEmail(emailStr)
  {
    var emailPat=/^(.+)@(.+)$/
    var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
    var validChars="\[^\\s" + specialChars + "\]"
    var quotedUser="(\"[^\"]*\")"
    var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
    var atom=validChars + '+'
    var word="(" + atom + "|" + quotedUser + ")"
    var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
    var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
    var matchArray=emailStr.match(emailPat)
    if (matchArray==null) {
      return false
    }
    var user=matchArray[1]
    var domain=matchArray[2]
    // See if "user" is valid
    if (user.match(userPat)==null) {
      return false
    }
    var IPArray=domain.match(ipDomainPat)
    if (IPArray!=null) {
      // this is an IP address
      for (var i=1;i<=4;i++) {
        if (IPArray[i]>255) {
            return false
        }
      }
        return true
    }
    var domainArray=domain.match(domainPat)
    if (domainArray==null) {
        return false
    }
    var atomPat=new RegExp(atom,"g")
    var domArr=domain.match(atomPat)
    var len=domArr.length
    if (domArr[domArr.length-1].length<2 ||
        domArr[domArr.length-1].length>3) {
       return false
    }
    if (len<2)
    {
       return false
    }
    return true;
}
 
$( function() {
      $( ".datepicker_arr" ).datepicker({
         minDate: '0' ,
         dateFormat: 'dd/mm/yy' 
      });
    });
 $( function() {
      $( ".datepicker_dep" ).datepicker({
         minDate: '0' ,
         dateFormat: 'dd/mm/yy' 
      });
    });
$(document).ready(function(){
  $('#ip_datearr').change(function(){
    $( ".datepicker_dep" ).datepicker('option', 'minDate', $( ".datepicker_arr" ).datepicker('getDate'));
    if($('#ip_datedep').val() == ''){
      $('#ip_datedep').val($('#ip_datearr').val());
    }
  });
});

/*Hiển thị số phòng còn trống đối với từng danh mục phòng*/
$(document).ready(function(){
  $(".loai_phong").change(function(){
    var id = $(".loai_phong").val();
    $.post("data.php", {id: id}, function(data){
      $(".so_phong").html(data);
    })
  })
})