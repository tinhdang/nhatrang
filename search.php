<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/functions/dbconnect.php';?>
<div class="content_main checkout_main">
  <div class="checkout container">
    <h3 >TÌM KIẾM</h3>
    <h4>Tìm thông tin người đặt</h4>
    <form method="post" action="" id="form_search">
      <div class="row form-group">
        <label class="control-label col-sm-3 col-sm-offset-1">Họ và tên <span style="color: red">(*)</span></label>
        <input class="col-sm-4" id="" type="text" name="username" value="">
      </div>
      <div class="row form-group">
        <label class="control-label col-sm-3 col-sm-offset-1">Số CMND <span style="color: red">(*)</span></label>
        <input class="col-sm-4" id="" type="number" name="idcard" value="">
      </div>
      <div class="row form-group send">
        <input type="submit" class="send_sm" name="search" value="Search">
      </div>
    </form>
     <?php
      if(isset($_POST['search'])){
        $username=$connect->real_escape_string($_POST['username']);
        $id_card=$connect->real_escape_string($_POST['idcard']);
        $con=mysqli_connect("localhost","root","","nhatrang");
        mysqli_set_charset($con,"utf8");
        $sql="SELECT * FROM booking WHERE name='$username' AND id_card='$id_card' ORDER BY id_bk DESC";
        $result = mysqli_query($con,$sql);
        $num = mysqli_num_rows($result);
          if($num==0){
              echo '<span style="color:red;font-weight:bold;">Không tồn tại kết quả được tìm kiếm<span>';
            }else {
              ?>
    <div class="form_checkout">
      <h4>Thông tin đặt phòng  </h4>
        <div class="row form-group">
          <div class="table-responsive">
            <table class="w3-table-all w3-hoverable">
              <thead>
                <tr class="w3-light-grey" style="font-weight: bold;">
                  <th>Id_bk</th>
                  <th>Tên KH</th>
                  <th>Địa chỉ</th>
                  <th>Điện thoại</th>
                  <th>Số phòng </th>
                  <th>Loại phòng </th>
                  <th>Thời gian </th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php
                while($arr_search=mysqli_fetch_assoc($result))
                {
                  $id_bk=$arr_search['id_bk'];
                  $id_room=$arr_search['id_room'];
                  $rooms=$arr_search['rooms'];
                  $arr_date=$arr_search['arr_date'];
                  $dep_date=$arr_search['dep_date'];
                  $num_children=$arr_search['num_children'];
                  $num_adult=$arr_search['num_adult'];
                  $name=$arr_search['name'];
                  $gender=$arr_search['gender'];
                  $id_card=$arr_search['id_card'];
                  $address=$arr_search['address'];
                  $email=$arr_search['email'];
                  $phone=$arr_search['phone'];
                  $note=$arr_search['note'];
                  $status=$arr_search['status'];
                 //Tách ngày tháng năm     
                  $arr_new=explode ('-', $arr_date);
                  $dep_new=explode ('-', $dep_date);
                  /*Đưa về định dạng ngày tháng năm*/
                  $nam_arr=$arr_new[0];
                  $thang_arr=$arr_new[1];
                  $ngay_arr=$arr_new[2];
                  $nam_dep=$dep_new[0];
                  $thang_dep=$dep_new[1];
                  $ngay_dep=$dep_new[2];
                  $arr_arr=array(
                    '0' =>$ngay_arr ,  
                    '1' =>$thang_arr , 
                    '2' =>$nam_arr , 
                    );
                  $arr_dep=array(
                    '0' =>$ngay_dep ,  
                    '1' =>$thang_dep, 
                    '2' =>$nam_dep, 
                    );
                  $checkin=implode('-',$arr_arr);
                  $checkout=implode('-',$arr_dep);
                  if($status==0){
                    $sta='Chưa trả';
                  }else{
                    $sta='Đã trả';
                  }
                ?>
                <tr>
                  <td><?php echo $id_bk;?></td>
                  <td><?php echo $name;?></td>
                  <td><?php echo $address;?></td>
                  <td><?php echo $phone;?></td>
                  <td><?php echo $rooms;?></td>
                    <?php 
                      $sqli="SELECT * FROM cate_room WHERE id_cate='$id_room'";
                      $res=$connect->query($sqli);
                      $row=mysqli_fetch_assoc($res);
                      $type_room=$row['type_room'];
                    ?>
                  <td><?php echo $type_room;?></td>
                  <td><?php echo 'Từ ngày '.$checkin.' đến ngày '.$checkout;?></td>
                  <td><?php echo $sta;?></td>
                </tr>
                <?php  } ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>
       <?php
          }
      }
    ?>
  </div>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/footer.php';?>

