<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/header.php';?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/functions/dbconnect.php';?>

<!--  Lấy booking pageindex-->
<?php 
  $arrival=$departure=$name_adult=$name_children= $arr_err=$dep_err='';
  if(isset($_POST['sb_booking'])){
    $arrival=$_POST['date_arr'];
    $departure=$_POST['date_dep'];
    $name_children=$_POST['name_children'];
    $name_adult=$_POST['name_adult'];
   }
?>
<div class="content_main booking_main" >
  <div class="booking container ">
    <h3>BOOKING</h3>
    <form method="post" id="form_booking" name="booking_room" action="" class="w3-container form-horizontal center-block">
      <div class="left"> 
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Họ và tên <span style="color: red">(*)</span></label>
          <input class="col-sm-8 col-xs-12" id="ip_name" type="text" name="username_bk" value="">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="name_error"></span>
        </div>
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Số CMND <span style="color: red">(*)</span></label>
          <input class="col-sm-8 col-xs-12" id="ip_card" type="number" name="card_bk" value="">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="card_error"></span>
        </div>
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Giới tính <span style="color: red">(*)</span></label>
          <input type="radio" name="gender_bk" id="ip_gender" checked value="Nam"> Nam &nbsp
          <input type="radio" name="gender_bk" id="ip_gender" value="Nữ">&nbsp Nữ
<!--           <span class="error_msg col-sm-8 col-sm-offset-3" id="gender_error"></span>
 -->        </div>
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Địa chỉ <span style="color: red">(*)</span></label>
          <input class="col-sm-8 col-xs-12" id="ip_address" type="text" name="address_bk" value="">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="address_error"></span>
        </div>
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Số điện thoại <span style="color: red">(*)</span></label>
          <input class="col-sm-8 col-xs-12" id="ip_phone" type="text" name="phone_bk" value="">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="phone_error"></span>
        </div>
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Email <span style="color: red">(*)</span></label>
          <input class="col-sm-8 col-xs-12" id="ip_email" type="text" name="email_bk" value="">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="email_error"></span>
        </div>
      </div>
      <div class="right">
        <div class="row form-group">
         
          <label class="control-label col-sm-3 col-xs-12">Loại phòng <span style="color: red">(*)</span></label>
          <?php 
              $sql_sl="SELECT * FROM cate_room";
              $result=$connect->query($sql_sl);            
            ?>
          <select  name="type_bk" id="ip_type" class="loai_phong col-sm-8" >
           <option value="">-->Chọn<--</option>
            <?php  while($row = mysqli_fetch_assoc($result)){
                $id_cate=$row['id_cate'];
                $name_room=$row['type_room'];
                $total_rooms=$row['total_rooms'];
                $room_bl=$row['room_bl'];

                  
              ?>
            <option value="<?php echo $id_cate;?>"><?php echo $name_room;?></option>
           
            <?php  } 
            ?>
          </select>
          <span class="error_msg col-sm-8 col-sm-offset-3" id="type_error"></span>
        </div>
        <!--  -->
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Chọn số phòng</label>
          <select  required="required" class="so_phong" id="select ip_rooms" name="rooms_bk">
            <option>Chọn số phòng</option>
          </select>
          <span class="error_msg col-sm-8 col-sm-offset-3" id="room_error"></span>
        </div>
        <div class="row form-group">
          <label class="control-label col-sm-2 col-sm-offset-1 col-xs-6">Người lớn <span style="color:red">(*)</span></label>
          <input class="col-sm-3 col-xs-6" id="ip_adult" type="text" name="people_adult" value="<?php echo $name_adult;?>">
         <span id=""></span>
           <label class="control-label col-sm-2 col-xs-6">Trẻ em </label>
          <input class="col-sm-3 col-xs-6" id="ip_children" type="text" name="people_children" value="<?php echo $name_children;?>">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="adult_error"></span>
        </div>
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Ngày đến <span style="color: red">(*)</span></label>
          <input class="col-sm-8 col-xs-12 datepicker_arr" id="ip_datearr" readonly="true" type="text" name="datearr_bk" placeholder="Ngày đến" value="<?php echo $arrival;?>">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="datearr_error"><?php echo $arr_err;?></span>
        </div>
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12">Ngày đi <span style="color: red">(*)</span></label>
          <input class="col-sm-8 col-xs-12 datepicker_dep" id="ip_datedep" type="text" readonly="true" name="datedep_bk" placeholder="Ngày đi" value="<?php echo $departure;?>">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="datedep_error"<?php echo $dep_err;?>></span>
        </div>
        <div class="row form-group">
          <label class="control-label col-sm-3 col-xs-12 ">Ghi chú <span style="color: red">(*)</span></label>
          <input class="col-sm-8 col-xs-12" id="ip_note" type="text" name="note_bk" value="">
          <span class="error_msg col-sm-8 col-sm-offset-3" id="note_error"></span>
        </div>
      </div>
      <div class="row form-group send">
        <input type="submit" class="send_sm" name="submit_bk" value="Gửi DL">
        <input type="reset" name="" class="re_sm" value="Reset">
      </div>
    </form>
  </div>
</div>
  <?php 

    if(isset($_POST['submit_bk']))
    {
      $username_bk=$connect->real_escape_string($_POST['username_bk']);
      $gender=$_POST['gender_bk'];
      $card_bk=$_POST['card_bk'];
      $address_bk=$connect->real_escape_string($_POST['address_bk']);
      $phone_bk=$_POST['phone_bk'];
      $email_bk=$connect->real_escape_string($_POST['email_bk']);
      $rooms_bk=$_POST['rooms_bk'];
      $people_adult=$_POST['people_adult'];
      $people_children=$_POST['people_children'];
      $datearr_bk=$_POST['datearr_bk'];
      $datedep_bk=$_POST['datedep_bk'];
      $status=0;
      $note_bk=$connect->real_escape_string($_POST['note_bk']);
      //Tách ngày tháng năm     
       $id_type=$_POST['type_bk'];

      $datearr_bkn=explode ('/', $datearr_bk);
      $datedep_bkn=explode ('/', $datedep_bk);
      /*Đưa về định dạng năm-tháng-ngày theo mysql*/
      $ngay_arr=$datearr_bkn[0];
      $thang_arr=$datearr_bkn[1];
      $nam_arr=$datearr_bkn[2];
      $ngay_dep=$datedep_bkn[0];  
      $thang_dep=$datedep_bkn[1];
      $nam_dep=$datedep_bkn[2];
      $arr_arr=array(
        '0' =>$nam_arr ,  
        '1' =>$thang_arr , 
        '2' =>$ngay_arr , 
        );
      $arr_dep=array(
        '0' =>$nam_dep ,  
        '1' =>$thang_dep, 
        '2' =>$ngay_dep, 
        );
      $date_arr=implode('-',$arr_arr);
      $date_dep=implode('-',$arr_dep);
      $date_current=date('Y-m-d');

      /*Kiểm tra còn phòng trống khi submit*/
        $sql1="SELECT* FROM cate_room WHERE id_cate=$id_type";
        $re_tsd=$connect->query($sql1);
        $arr_tsd=mysqli_fetch_assoc($re_tsd);
        $room_blank=$arr_tsd['room_bl'];
        $check=$room_blank-$rooms_bk;
        if($check<0){
           echo '<script type="text/javascript">alert("Số phòng bạn nhập vượt quá số phòng còn trống, vui lòng chọn lại");</script>';
          exit();
        }else {
         // tính số phòng còn trống sau khi được đặt, rồi update lại table
          $update="UPDATE cate_room SET room_bl=$check WHERE id_cate=$id_type";
          $re_up=$connect->query($update);
        }
      
      $sql = "INSERT INTO booking (id_room, rooms, arr_date,dep_date,num_children,num_adult,name,gender,id_card,address,email,phone,note,status)
      VALUES ('$id_type','$rooms_bk','$date_arr','$date_dep','$people_children' ,'$people_adult','$username_bk','$gender','$card_bk',N'$address_bk','$email_bk','$phone_bk',N'$note_bk','0')";
      if ($connect->query($sql) === TRUE) {
     /*   echo '<script type="text/javascript">alert("Cảm ơn bạn đã đặt phòng. Chúng tôi sẽ liên hệ với bạn");</script>';*/
          echo 'Đặt phòng thành công';
        } else {
        echo "Error: " . $sql . "<br>" . $con->error;
        }
        $connect->close();
  }
  ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/nhatrang/templates/footer.php';?>
