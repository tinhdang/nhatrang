<div class="footer_up">
    <div class="container">
      <div class="col-sm-1"></div>
      <div class="footer_title hotel col-sm-1">
        <h4 class="p_title title_1">Hotel</h4>
          <ul class="ul_li ul_li_1">
            <li>Booking</li>
            <li>About Us</li>
            <li>Location</li>
            <li>Contact</li>
          </ul>
      </div>
      <div class="footer_title accomdation col-sm-2">
        <h4 class="p_title title_2">Accommodation</h4>
          <ul class="ul_li ul_li_2">
            <li>Room</li>
            <li>Equipment</li>
            <li>Services</li>
            <li>Safery</li>
          </ul>
      </div>
      <div class="footer_title service col-sm-2">
        <h4 class="p_title title_3">Service</h4>
          <ul class="ul_li ul_li_3">
            <li>Accommodation</li>
            <li>Conferences</li>
            <li>Gastronomy</li>
            <li>Relaxtion</li>
          </ul>
      </div>
      <div class="footer_title menu col-sm-2">
        <h4 class="p_title title_4">Menu</h4>
          <ul class="ul_li ul_li_4">
            <li>Menu restaurant</li>
            <li>Business menu</li>
            <li>Lobby bar menu</li>
            <li>&nbsp;</li>
          </ul>
      </div>
      <div class="footer_title contactus col-sm-3">
       <h4 class="p_title title_5">Contact Us</h4>
          <ul class="ul_li ul_li_5">
            <li>15 B Nguyen Thien Thuat, Nha Trang</li>
            <li>Tell: +84.510.3864610</li>
            <li>Fax: +84.510.3862334</li>
            <li>Location on Google map</li>
          </ul>
      </div>
      <div class="col-sm-1"></div>
      <div class="bottom">
        <div class="container">
        <div class="copy_left  col-sm-4 col-xs-12">
            <p>Copyright &copy; 2013 Vitour - All right Reseved</p>
          </div>
          <div class="copy_right col-sm-4 col-sm-offset-2 col-xs-12">
            <img src="image/paypal.png" alt="paypal">
            <img src="image/visa.png" alt="visa">
            <img src="image/express.png" alt="express">
            <img src="image/master.png" alt="master">
          </div>
        </div>
        <div class="message">
          <p class="click_mess"> Leave a Message</p>
          <div class="form_message">
            <form name="form_mess" action="" method="post" onsubmit="return ValidateMess();">
                <h4> Gửi message</h4>
                <label class="user_mess">Người gửi (Email)</label>
                 <input id="id1" type="text" name="user_mess" value="">
                <label class="content_mess">Nội dung</label>
                  <input type="text" name="content_mess" value="">
                <label class="submit_mess"></label>
                <input type="submit" name="gui_mess" value="Gửi">
            </form>
          </div>
        </div>
        <?php 
          if(isset($_POST['gui_mess'])){
            $mail_send=$_POST['user_mess'];
            $mail_content=$_POST['content_mess'];
          require_once './phpmailer/class.smtp.php';
          require_once './phpmailer/class.phpmailer.php';

          $mail = new PHPMailer();
          $mail->IsSMTP(); 
          $mail->SMTPAuth = true; 
          $mail->Host = "tls://smtp.gmail.com:587"; 
          $mail->Username = "tinh10dt2@gmail.com"; 
          $mail->Password = "tinhtinhtinh"; 
          $mail->SMTPSecure = "TLS";
          $mail->IsHTML(true);
          $mail->SMTPDebug = 0;
          $mail->CharSet = "utf-8";

          $mail->SetFrom($mail_send,'Khách');
          $mail->AddAddress("tinh10dt2@gmail.com"); 
          $mail->Subject = "Contact";
          $body = "Đây là nội dung liên hệ được gửi từ : <b>".$mail_send." </b>. </br>Với nội dung : <b>".$mail_content."</b>";
          $mail->Body = $body;
          if(!$mail->Send()) {
          echo "Lỗi !!!!!!" . $mail->ErrorInfo;
          } else {
          echo "Đã gửi thành công !!";
          } 
} 
  ?>
      </div>

    </div>
  </div>
  <script type="text/javascript" src="libraries/jquery-3.1.1.min.js"></script>
  <script src="libraries/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script type="text/javascript" src="libraries/my.js"></script>

</body>
</html>