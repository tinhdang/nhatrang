<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Nha Trang Palace</title>
  <link rel="stylesheet" type="text/css" href="css/mystyle.css">
  <link rel="stylesheet" type="text/css" href="css/reset.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="libraries/jquery-ui-1.12.1.custom/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js" type="text/javascript"></script>
  <script src="libraries/ajax.js" type="text/javascript"></script>
</head>
<body>
  <div class="header">
    <div class="top_header">
      <div class="menu_search container">
        <select class="select_menu">
          <option>ENGLISH</option>
        </select>   
        <input class="input_menu"  type="text" placeholder="Search" name="">
      </div>
      <div class="logo ">
        <img src="image/logo-cut.png" class="img_logo img-rensponsive img_max">
        <img src="image/sm-icon.png" class="img_logo img-rensponsive img_min">
      </div>
    </div>   
    <nav class="navbar navbar-inverse menu_bar" id="navbar_menu">
      <div class="form_min">
        <form class="form_order">
          <button type="button" class="btn btn-default form_min">
             BOOKING
          </button> 
        </form>
      </div>
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle " id="button_menu" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav" id="menu_ul">
            <li id="acti" id="menu_1"><a href="index.php">Trang chủ</a></li>
            <li id="menu_2"><a href="#">Giới thiệu</a></li>
            <li id="menu_2" class="dropdown ">
              <a data-toggle="dropdown" id="menu_2" href="" id="acount">Loại phòng</a>
              <ul class="dropdown-menu">
                <li><a href="#">Palace Club Suite</a></li>
                <li><a href="#">Palace Suite</a></li>
                <li><a href="#">Deluxe Room</a></li>
                <li><a href="#">Superior SeaView Room</a></li>
                <li><a href="#">Superior SeCityView Room</a></li>
              </ul>
            </li>
            <li id="menu_3"><a href="#">Dịch vụ</a></li>
            <li id="menu_4 dropdown">
                <a data-toggle="dropdown" href="" id="menu_4" id="acount">Nhà hàng - Bar</a>
                <ul class="dropdown-menu">
                    <li><a href="#">Nhà hàng Royal</a></li>
                    <li><a href="#">Nhà hàng Sliver Sea</a></li>
                    <li><a href="#">Nhà hàng Palace</a></li>
                    <li><a href="#">Place Cafe</a></li>
                    <li><a href="#">Lobby Bar</a></li>
                    <li><a href="#">Moon Bar</a></li>
                </ul>
            </li>
            <li id="menu_5"><a href="#">Hình ảnh</a></li>
            <li id="menu_6"><a href="#">Tin tức</a></li>
            <li id="menu_7"><a href="checkout.php">Checkout</a></li>
            <li id="menu_8"><a href="search.php">Search</a></li>
             <li id="menu_8 dropdown">
                <a data-toggle="dropdown" href="" id="acount">Tài khoản</a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="register_user">Đăng ký</a></li>
                    <li><a href="#" class="login_user">Đăng nhập</a></li>
                </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!--  login register-->
    <div class="login_form">
      <form name="form_login" method="post" class="">
        <h4>Login</h4>
        <label>Username</label>
        <input type="text" name="user_login">
        <label>Password</label>
        <input type="Password" name="pass_login">
        <input type="submit" name="" value="Login">
      </form>
      <span class="caret-inner"> </span>
    </div>
  </div>